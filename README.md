# TreeSpeechGenerator #
[DOWNLOAD [45Kb]](https://bitbucket.org/lbrocklehurst/treespeechgenerator/downloads/TreeSpeechGenerator_v1.0.0.0.zip)
### v1.0.0.0 ###
A handy program to generate TreeSpeech Dialogue Tree (.tspeech) files for use within the [TreeSpeech Dialogue Tree Framework](https://bitbucket.org/lbrocklehurst/treespeech)

## Features ##

* Quickly and Easily put together and generate a standard XML formatted Dialogue Tree file
* Generated file is instantly compatible with the [TreeSpeech Dialogue Tree Framework](https://bitbucket.org/lbrocklehurst/treespeech)

## How to Use ##

### Create and Add Your Entry ###
Fill in your Dialogue Entry's details or use **Edit Entry** to edit an existing Entry

* ID number
* * This can be any number, but two entries cannot have the same ID

* Text
* * Your Entry text goes here - maximum 80 characters

* Tag
* * Select your tag here (if any) - the main ones being *START* (should always be one of these) and *RESPONSE* for use with response text

* Continue To/Pointer
* * This is the Entry this Entry/Response should follow onto

* Responses
* * Add to this list with the available Responses to this Entry
* * All Entries marked with the RESPONSES tag will display here

### Save Your Dialogue File ###
Using **File > Save** will save your .tspeech file to your computer to be used with the TreeSpeech Framework