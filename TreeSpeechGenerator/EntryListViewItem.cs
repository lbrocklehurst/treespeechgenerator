﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TreeSpeech;

namespace TreeSpeechGenerator
{
    class EntryListViewItem : ListViewItem
    {
        /// <summary>
        /// Initialise an EntryListViewItem object which holds information from an Entry
        /// </summary>
        public EntryListViewItem(Entry entry)
        {
            this.Text = entry.ID.ToString(); // ID
            this.SubItems.Add(new ListViewSubItem(this, entry.Text)); // Text
            string responses = "";
            if (entry.Responses != null)
                for (int i = 0; i < entry.Responses.Length; i++)
                    responses += " " + entry.Responses[i].ToString();
            this.SubItems.Add(new ListViewSubItem(this, responses)); // Responses
            this.SubItems.Add(new ListViewSubItem(this,
                (entry.Pointer > -1) ? entry.Pointer.ToString() : ""
                )); // 'Continue to'
            this.SubItems.Add(new ListViewSubItem(this, entry.TAG.ToString())); // Tag
        }
    }
}
