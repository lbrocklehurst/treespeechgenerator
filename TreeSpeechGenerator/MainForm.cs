﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TreeSpeech;

namespace TreeSpeechGenerator
{
    public partial class MainForm : Form
    {
        Dialogue dialogue = new Dialogue();                                         // Main Dialogue file
        Entry currentEntry = new Entry();                                           // Currently editing Entry
        List<int> tempResponses = new List<int>();                                  // Temporary list to store responses before inserting
        bool ID_CORRECT = true, POINTERID_CORRECT = true, TEXT_CORRECT = true;
        private bool editmode = false;

        bool EDIT_MODE
        {
            get { return editmode; }
            set
            {
                editmode = value;
                Event_EditMode();
            }
        }

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //Dummy_Data();
            Init_Tags();
            RefreshAll();
            SetFirstAvailableID();
        }
        /// <summary>
        /// Set the ID field to the first available ID number
        /// </summary>
        private void SetFirstAvailableID()
        {
            int idcheck = 1;
            while(IDExists(idcheck))
                idcheck++;
            txtID.Text = idcheck.ToString();
        }
        /// <summary>
        /// Fill the Tag ComboBox with all available tags
        /// </summary>
        private void Init_Tags()
        {
            this.dropTag.Items.Clear();
            foreach (TreeSpeech.Globals.Tags tag in Enum.GetValues(typeof(TreeSpeech.Globals.Tags)))
                this.dropTag.Items.Add(tag);
            if (this.dropTag.Items != null && this.dropTag.Items.Count > 0)
                this.dropTag.SelectedIndex = 0;
        }
        /// <summary>
        /// Call Refresh on all relevant fields/elements
        /// </summary>
        private void RefreshAll()
        {
            RefreshResponseDropdown();
            RefreshResponseList();
            RefreshPointersDropdown();
            RefreshEntriesList();
            ResetTagsDropdown();
            CheckID(null, EventArgs.Empty);
            CheckText(null, EventArgs.Empty);
        }
        /// <summary>
        /// Refresh ComboBox with list of Responses
        /// </summary>
        private void RefreshResponseDropdown()
        {
            this.dropResponses.Items.Clear();
            foreach(Entry entry in dialogue.Tree.Entries)   // Loop through Entries in Tree
            {
                if (entry.TAG == TreeSpeech.Globals.Tags.RESPONSE)     // Check the Tag is RESPONSE
                    this.dropResponses.Items.Add(new ComboBoxItem(string.Format("{0} - {1}", entry.ID, entry.Text), entry.ID));
            }
            if (this.dropResponses.Items.Count > 0)
                this.dropResponses.SelectedIndex = 0;
        }
        /// <summary>
        /// Refresh the List of Responses on the current Entry
        /// </summary>
        private void RefreshResponseList()
        {
            this.listResponses.Items.Clear();
            for(int i = 0; i < tempResponses.Count; i++)    // Loop through responses stored in the temporary List
            {
                Entry entry = dialogue.Tree.GetEntry(tempResponses[i]);                         // Get Entry from response ID
                this.listResponses.Items.Add(string.Format("{0} - {1}", entry.ID, entry.Text)); // Add Entry ID and name to list
            }
        }
        /// <summary>
        /// Refresh the ComboBox of Entries able to continue to
        /// </summary>
        private void RefreshPointersDropdown()
        {
            this.dropPointers.Items.Clear();
            foreach (Entry entry in dialogue.Tree.Entries)  // Loop through all of the entries within the Dialogue Tree
            {
                if (entry.TAG != TreeSpeech.Globals.Tags.RESPONSE) // Check that Entry is not a RESPONSE
                    this.dropPointers.Items.Add(new ComboBoxItem(string.Format("{0} - {1}", entry.ID, entry.Text), entry.ID));
            }
        }
        /// <summary>
        /// Refresh the List of Entries
        /// </summary>
        private void RefreshEntriesList()
        {
            this.listEntries.Items.Clear();
            foreach(Entry entry in dialogue.Tree.Entries)   // Loop through all the entries within the Dialogue Tree and add to List
                this.listEntries.Items.Add(new EntryListViewItem(entry));
        }
        /// <summary>
        /// Reset ComboBox of Tags (move selected index back to start)
        /// </summary>
        private void ResetTagsDropdown()
        {
            if (dropTag.Items != null && dropTag.Items.Count > 0)
                dropTag.SelectedIndex = 0;
        }
        /// <summary>
        /// Load data from current Entry into fields
        /// </summary>
        private void LoadFromCurrentEntry()
        {
            txtID.Text = currentEntry.ID.ToString();
            dropTag.SelectedIndex = dropTag.Items.IndexOf(currentEntry.TAG);
            rtxtText.Text = currentEntry.Text;
            RefreshPointersDropdown();
            for (int i = 0; i < dropPointers.Items.Count; i++)  // Loop through Pointers ComboBox items and find the one that matches Entry's ID
            {
                ComboBoxItem item = (ComboBoxItem)this.dropPointers.Items[i];
                if((int)item.Value == currentEntry.Pointer)
                {
                    dropPointers.SelectedIndex = i;
                    break;
                }
            }
            if (currentEntry.Responses != null)
                tempResponses = currentEntry.Responses.ToList();    // Put Responses array into temp List
            RefreshResponseDropdown();
            RefreshResponseList();
        }
        /// <summary>
        /// Check the ID within the ID field is valid and/or doesn't exist already
        /// </summary>
        private void CheckID(object sender, EventArgs e)
        {
            lblIDHelp.ForeColor = Color.Gray;
            lblIDHelp.Text = "(Only change if needed, numbers only)";
            ID_CORRECT = true;
            EDIT_MODE = false;
            int IDValue = 0;
            bool success = int.TryParse(txtID.Text, out IDValue);   // Parse value in field
            if(success) // field value is a number
            {
                if (IDValue <= 0)   // number is lower than 1
                {
                    lblIDHelp.ForeColor = Color.DarkRed;
                    lblIDHelp.Text = "ID needs to be a positive value!";
                    ID_CORRECT = false;
                }
                if(IDExists(IDValue))   // ID already exists
                {
                    lblIDHelp.ForeColor = Color.DarkRed;
                    lblIDHelp.Text = "ID already exists!";
                    EDIT_MODE = true;
                }
            }
            else    // Field value is not a number or empty
            {
                if (txtID.Text == "")   // Empty
                {
                    lblIDHelp.ForeColor = Color.DarkRed;
                    lblIDHelp.Text = "Please enter an ID!";
                    ID_CORRECT = false;
                }
                else
                {
                    lblIDHelp.ForeColor = Color.DarkRed;
                    lblIDHelp.Text = "Please enter numbers only!";
                    ID_CORRECT = false;
                }
            }
        }
        /// <summary>
        /// Check if specific ID exists already within Dialogue Tree
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool IDExists(int id)
        {
            if (dialogue.Tree.Entries.Find(x => x.ID == id) != null)
                return true;
            return false;
        }
        /// <summary>
        /// Event called whenever EditMode property is changed
        /// </summary>
        private void Event_EditMode()
        {
            if(EDIT_MODE)
                btnAddEntry.Text = "Save Changes";  // In edit mode, change button text
            else
                btnAddEntry.Text = "Add Entry";
        }

        //---------//
        // BUTTONS //
        //---------//
        /// <summary>
        /// Add Response Button Click
        /// </summary>
        private void btnAddResponse_Click(object sender, EventArgs e)
        {
            if (this.dropResponses.SelectedIndex < 0)   // No selection
                return;
            ComboBoxItem item = (ComboBoxItem)this.dropResponses.SelectedItem;  // Cast to ComboBoxItem
            if(tempResponses.Find(x => x == (int)item.Value) != 0)  // Check if this response is already in the list
            {
                MessageBox.Show("Response already in this list! Please choose another.");
                return;
            }
            tempResponses.Add((int)item.Value); // Add to temp List
            RefreshResponseList();
        }
        /// <summary>
        /// Delete Response Button Click
        /// </summary>
        private void btnDeleteResponse_Click(object sender, EventArgs e)
        {
            if (this.listResponses.SelectedIndex < 0)   // No selection
                return;
            tempResponses.RemoveAt(listResponses.SelectedIndex);    // Remove from list
            RefreshResponseList();
        }
        /// <summary>
        /// Add Entry Button Click
        /// </summary>
        private void btnAddEntry_Click(object sender, EventArgs e)
        {
            if(EDIT_MODE)   // In Edit mode, display a message to make sure the user knows
            {
                DialogResult result = MessageBox.Show("You are currently editing an existing entry.\nAre you happy with the changes?", "Save changes to Entry", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    EditEntrySave();
                    return;
                }
                return;
            }
            if(rtxtText.Text == "") // No text in Text field
            {
                MessageBox.Show("No text entered for this Entry!");
                return;
            }
            if(!ID_CORRECT || !POINTERID_CORRECT || !TEXT_CORRECT)  // Cannot continue
            {
                MessageBox.Show("There are errors in the form, please fix before submitting.");
                return;
            }
            AddEntry();
        }
        /// <summary>
        /// Delete Entry Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteEntry_Click(object sender, EventArgs e)
        {
            if (this.listEntries.SelectedIndices.Count <= 0)    // If nothing selected, return
                return;
            dialogue.Tree.Entries.RemoveAt(this.listEntries.SelectedIndices[0]);    // Remove Entry
            RefreshAll();
        }
        /// <summary>
        /// Edit Entry Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditEntry_Click(object sender, EventArgs e)
        {
            if (this.listEntries.SelectedIndices.Count <= 0)    // If nothing selected, return
                return;
            currentEntry = dialogue.Tree.Entries[this.listEntries.SelectedIndices[0]];  // Push Entry from list into current Entry
            LoadFromCurrentEntry();     // Load data from entry into fields
        }

        //--------------//
        // MENU BUTTONS //
        //--------------//
        /// <summary>
        /// New - Menu Button Click
        /// </summary>
        private void menuItemNew_Click(object sender, EventArgs e) { New(); }
        /// <summary>
        /// Open - Menu Button Click
        /// </summary>
        private void menuItemOpen_Click(object sender, EventArgs e) { Open(); }
        /// <summary>
        /// Save - Menu Button Click
        /// </summary>
        private void menuItemSave_Click(object sender, EventArgs e) { Save(); }
        /// <summary>
        /// Save As - Menu Button Click
        /// </summary>
        private void menuItemSaveAs_Click(object sender, EventArgs e) { SaveAs(); }
        /// <summary>
        /// Exit - Menu Button Click
        /// </summary>
        private void menuItemExit_Click(object sender, EventArgs e) { this.Close(); }
        /// <summary>
        /// Add Entry
        /// </summary>
        private void AddEntry()
        {
            SyncDataToEntry();                          // Push data from form into Entry object
            rtxtText.Text = "";                         // Reset Text field
            tempResponses = new List<int>();            // Reset responses list
            dialogue.Tree.Entries.Add(currentEntry);    // Add to Entries
            currentEntry = new Entry();                 // Clear Entry
            RefreshAll();
            SetFirstAvailableID();
        }
        /// <summary>
        /// Save data from currently editing Entry
        /// </summary>
        private void EditEntrySave()
        {
            SyncDataToEntry();  // Push data from form
            rtxtText.Text = "";
            tempResponses = new List<int>();
            Entry found = dialogue.Tree.GetEntry(currentEntry.ID);  // Find the Entry to replace
            if (found != null)      // If found, replace
                found = currentEntry;
            else
            {
                AddEntry();
                return;
            }
            currentEntry = new Entry();
            RefreshAll();
            SetFirstAvailableID();
        }
        /// <summary>
        /// Push data from Form elements into Entry object
        /// </summary>
        private void SyncDataToEntry()
        {
            currentEntry.ID = int.Parse(txtID.Text);
            currentEntry.Text = rtxtText.Text;
            currentEntry.TAG = (TreeSpeech.Globals.Tags)Enum.Parse(typeof(TreeSpeech.Globals.Tags), this.dropTag.Items[this.dropTag.SelectedIndex].ToString());

            if (this.dropPointers.SelectedIndex >= 0)  // See if the user has selected a Pointer/Continue Entry
            {
                ComboBoxItem item = (ComboBoxItem)this.dropPointers.SelectedItem;   // Cast to ComboBoxItem
                if (item != null)
                    currentEntry.Pointer = (int)item.Value; // Take int value from ComboBoxItem (pointer)
            }

            currentEntry.Responses = tempResponses.ToArray();   // Convert temporary responses list to an Array to store
        }
        /// <summary>
        /// Check and update the 'Text' field
        /// </summary>
        private void CheckText(object sender, EventArgs e)
        {
            lblChars.ForeColor = Color.Gray;
            TEXT_CORRECT = true;
            int charCount = rtxtText.Text.Length;   // Get the character count
            string hintText = "";
            if(charCount > Globals.MaximumCharacters)  // Character count is above 80
            {
                lblChars.ForeColor = Color.DarkRed;
                TEXT_CORRECT = false;   // Turn error check to false
                hintText = " !!!";      // Add hint text
            }
            lblChars.Text = string.Format("{0}/80{1}", charCount, hintText);    // Display label text with current char count
        }
        /// <summary>
        /// New Dialogue file - Clear elements
        /// </summary>
        private void New()
        {
            DataHelper.New();                   // Call New() on DataHelper
            dialogue = new Dialogue();          // Clear out Dialogue object
            tempResponses = new List<int>();    // Clear Temp Responses
            rtxtText.Text = "";
            RefreshAll();
            SetFirstAvailableID();
        }
        /// <summary>
        /// Open a Dialogue file
        /// </summary>
        private void Open(string location = "")
        {
            Dialogue newDialogue;
            if (location == "")         // If no location provided, use Open() on Dialogue
                newDialogue = DataHelper.Open();
            else
                newDialogue = DataHelper.Open(location);

            if (newDialogue == null)    // Problem opening, don't do anything
                return;

            dialogue = newDialogue;
            RefreshAll();
            SetFirstAvailableID();
        }
        /// <summary>
        /// Save Dialogue file
        /// </summary>
        private void Save()
        {
            Dialogue savedDialogue = DataHelper.Save(dialogue); // Call save on DataHelper
            if (savedDialogue == null)
                Debug.WriteLine("Failed to save file.");
        }
        /// <summary>
        /// Save As Dialogue file
        /// </summary>
        private void SaveAs()
        {
            Dialogue savedDialogue = DataHelper.SaveAs(dialogue);
            if (savedDialogue == null)
                Debug.WriteLine("Failed to save file.");
        }

        /// <summary>
        /// Creates a set of dummy data (Use in Form_Load)
        /// </summary>
        protected void Dummy_Data()
        {
            Entry e1 = new Entry(1, "First entry");
            Entry e2 = new Entry(2, "Second entry with lots of text on a single line, I guess.");
            e2.TAG = TreeSpeech.Globals.Tags.RESPONSE;
            Entry e3 = new Entry(3, "Another entry");
            Entry e4 = new Entry(4, "Fourth entry");
            e4.TAG = TreeSpeech.Globals.Tags.RESPONSE;
            dialogue.Tree.Entries.Add(e1);
            dialogue.Tree.Entries.Add(e2);
            dialogue.Tree.Entries.Add(e3);
            dialogue.Tree.Entries.Add(e4);
        }
    }
}
