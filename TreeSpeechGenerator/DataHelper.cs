﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TreeSpeech;

namespace TreeSpeechGenerator
{
    public class DataHelper
    {
        /// <summary>
        /// New file
        /// </summary>
        public static void New() { FileStatus.CurrentFile = ""; }
        /// <summary>
        /// Open up Dialogue file
        /// </summary>
        public static Dialogue Open()
        {
            OpenFileDialog dlg = new OpenFileDialog(); // Create OpenFileDialog

            /* Set filter for file extension */
            dlg.Filter = string.Format("Dialogue file (*{0})|*{0}", TreeSpeech.Globals.Filetype);

            DialogResult result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                FileStatus.CurrentFile = dlg.FileName;
                return new Dialogue(dlg.FileName);
            }
            else
                Debug.WriteLine("'Open' browser window closed.");
            return null;
        }
        /// <summary>
        /// Open up Dialogue file from location
        /// </summary>
        public static Dialogue Open(string location)
        {
            Debug.WriteLine("Opening '" + location + "'...");
            Dialogue dialogue = new Dialogue(location);
            if (dialogue != null)
            {
                FileStatus.CurrentFile = location;
                return dialogue;
            }

            MessageBox.Show("Failed to load Dialogue!", "Dialogue failed to load", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return null;
        }
        /// <summary>
        /// Save Dialogue file
        /// </summary>
        public static Dialogue Save(Dialogue dialogue)
        {
            Debug.WriteLine("Saving Dialogue...");

            if (FileStatus.CurrentFile == "")
            {
                return SaveAs(dialogue);
            }
            if (!dialogue.Save(FileStatus.CurrentFile))
            {
                MessageBox.Show(string.Format("Failed to save Dialogue! {0} doesn't exist!", Path.GetFileName(FileStatus.CurrentFile)),
                    "Failed to save Dialogue", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return SaveAs(dialogue);
            }
            return dialogue;
        }
        /// <summary>
        /// Save As Dialogue file
        /// </summary>
        public static Dialogue SaveAs(Dialogue dialogue)
        {
            Console.WriteLine("Save As...");

            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.Filter = string.Format("Dialogue file (*{0})|*{0}", TreeSpeech.Globals.Filetype);
            DialogResult result = savedialog.ShowDialog();

            if (result != DialogResult.OK || savedialog.FileName == "") // Save box was closed or left empty
                return null;
            FileStatus.CurrentFile = savedialog.FileName;
            return Save(dialogue);
        }
    }
}
